<!-- video -->
<div class="modal modal__video fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="images/iconos/cerrar.png" alt="">
        </button>
        <iframe src="https://www.youtube.com/embed/nbdDvOfUNhs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>

<!-- descripcion 360 -->
<div class="modal modal__descrip fade" id="ModalDescripcion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="images/iconos/cerrar.png" alt="">
        </button>
        <div class="container-fluid p-0">
          <div class="row">
            <div class="col-12">
              <div class="modal__descrip__tabs">
                <ul class="nav nav-tabs">
                   <li><a data-toggle="tab"  class="active primero"  href="#loft">Loft</a></li>
                   <li><a data-toggle="tab" href="#duo">Duo</a></li>
                   <li><a data-toggle="tab" href="#social">Social</a></li>
                   <li><a data-toggle="tab" href="#areaC">Áreas comunes</a></li>
                 </ul>
                 <div class="tab-content modal__descrip__tabs__content">
                   <div id="loft" class="tab-pane fade in active show">
                     <iframe src='https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/' frameborder='0' allowfullscreen='allowfullscreen'></iframe>
                   </div>
                   <div id="duo" class="tab-pane fade">
                     <iframe src="https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/"></iframe>
                   </div>
                   <div id="social" class="tab-pane fade">
                     <iframe src="https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/"></iframe>
                   </div>
                   <div id="areaC" class="tab-pane fade">
                     <iframe src="https://virtualexperience.pe/3d-model/recorrido-virtual-senda-inmobiliaria-proyecto-audacity/fullscreen/"></iframe>
                   </div>
                 </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- modelo -->
<div class="modal modal__modelo fade" id="ModalModelo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body modal__modelo__body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="images/iconos/cerrar.png" alt="">
        </button>
        <div class="container p-0">
          <div class="row">
            <div class="col-xl-4 col-lg-6 p-0">
              <div class="modal__modelo__body__texto d-flex align-items-center justify-content-center">
                <!-- <h3>departamentos</h3> -->
                <div class="modal__modelo__body__texto__in">
                  <h2>único</h2>
                  <span>Da el primer paso, conquista tu independencia para dominar la ciudad. </span>
                  <div class="dropdown">
                    <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      DUO TIPO 1
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                      <a class="dropdown-item" href="#">Lima</a>
                      <a class="dropdown-item" href="#">Another action</a>
                      <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                  </div>
                  <table>
                    <tbody>
                     <tr>
                       <td><img src="images/iconos/area.png" alt=""> Área total</td>
                       <td class="numero">53m2</td>
                     </tr>
                     <tr>
                       <td><img src="images/iconos/dormitorio.png" alt=""> Habitaciones</td>
                       <td class="numero">3</td>
                     </tr>
                     <tr>
                       <td><img src="images/iconos/banio.png" alt=""> Baños</td>
                       <td class="numero">2</td>
                     </tr>
                     <tr>
                       <td><img src="images/iconos/cocina.png" alt=""> Cocina</td>
                       <td class="numero">Abierta</td>
                     </tr>
                   </tbody>
                  </table>
                  <a href="#" class="buttom buttom--rellenoV asesorateI"><svg width="36" height="28" viewBox="0 0 36 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M20.9514 18.7835C23.0673 17.8916 24.7372 16.6775 25.9609 15.1408C27.1848 13.6045 27.7968 11.9314 27.7968 10.1214C27.7968 8.31145 27.1848 6.63842 25.9609 5.1018C24.7372 3.56538 23.0673 2.35117 20.9514 1.45904C18.8355 0.567043 16.5341 0.121216 14.0469 0.121216C11.5599 0.121216 9.25857 0.567317 7.14252 1.45931C5.02673 2.35131 3.35678 3.56552 2.133 5.102C0.908938 6.63842 0.296875 8.31138 0.296875 10.1214C0.296875 11.6708 0.759599 13.1298 1.68395 14.4967C2.60831 15.8639 3.87793 17.0165 5.4924 17.9538C5.36228 18.2666 5.22882 18.5527 5.09207 18.8135C4.95525 19.0738 4.79264 19.3243 4.60369 19.5654C4.41488 19.8065 4.26855 19.9952 4.16429 20.1314C4.0601 20.2681 3.89086 20.4604 3.65642 20.7076C3.42191 20.955 3.27209 21.1178 3.20717 21.1957C3.20717 21.1826 3.18097 21.2121 3.12898 21.2837C3.07678 21.3554 3.0475 21.3877 3.04093 21.3816C3.0345 21.3746 3.00837 21.4072 2.96288 21.4788C2.91732 21.5505 2.89447 21.5863 2.89447 21.5863L2.84555 21.6836C2.82613 21.7225 2.81299 21.7614 2.80656 21.8007C2.79999 21.8395 2.79671 21.8821 2.79671 21.9275C2.79671 21.973 2.80314 22.0151 2.81627 22.0544C2.84248 22.2234 2.91732 22.3599 3.04093 22.4646C3.16469 22.5686 3.29795 22.6205 3.4414 22.6205H3.50003C4.15095 22.5294 4.71095 22.4252 5.17963 22.3079C7.18486 21.787 8.99492 20.9536 10.6095 19.8079C11.7812 20.0162 12.9272 20.1204 14.0469 20.1204C16.5339 20.1212 18.8355 19.6755 20.9514 18.7835ZM10.0237 17.1529L9.16416 17.7581C8.79954 18.0053 8.39613 18.2596 7.95331 18.5202L8.63707 16.8793L6.74252 15.7856C5.49226 15.0566 4.52235 14.1972 3.83223 13.2075C3.14204 12.218 2.79712 11.1893 2.79712 10.1215C2.79712 8.79319 3.30841 7.54971 4.33046 6.39084C5.35237 5.23198 6.72939 4.31406 8.46118 3.63673C10.1928 2.95982 12.0549 2.62105 14.0471 2.62105C16.0392 2.62105 17.9014 2.95982 19.633 3.63673C21.3647 4.31399 22.7417 5.23198 23.7641 6.39084C24.7862 7.54964 25.2971 8.79313 25.2971 10.1215C25.2971 11.4496 24.7862 12.693 23.7641 13.8519C22.7417 15.0111 21.3647 15.9288 19.633 16.6058C17.9014 17.2829 16.0394 17.6214 14.0471 17.6214C13.0707 17.6214 12.0746 17.5301 11.0588 17.348L10.0237 17.1529Z" />
                  <path d="M33.9101 19.5055C34.8349 18.1444 35.2969 16.6833 35.2969 15.1206C35.2969 13.519 34.8086 12.0214 33.8324 10.6284C32.8557 9.23542 31.5274 8.07635 29.8478 7.15186C30.1471 8.12834 30.2968 9.11789 30.2968 10.1207C30.2968 11.8654 29.8612 13.519 28.9882 15.0817C28.1158 16.6439 26.8658 18.024 25.2382 19.2222C23.7277 20.3159 22.0089 21.1555 20.0817 21.7416C18.155 22.3274 16.1431 22.6206 14.0467 22.6206C13.6562 22.6206 13.0832 22.5948 12.3281 22.5428C14.9452 24.2612 18.0183 25.1208 21.5468 25.1208C22.6667 25.1208 23.8124 25.0164 24.9844 24.8081C26.5989 25.9543 28.409 26.7873 30.4141 27.3084C30.8829 27.4259 31.4428 27.5299 32.0939 27.6209C32.25 27.6343 32.3936 27.5884 32.5237 27.4844C32.6539 27.38 32.7386 27.2372 32.7775 27.0551C32.7714 26.9769 32.7775 26.934 32.7971 26.9278C32.8163 26.9216 32.8131 26.8791 32.7873 26.8011C32.7615 26.7228 32.7484 26.6838 32.7484 26.6838L32.6997 26.5863C32.6862 26.5606 32.6641 26.5246 32.6314 26.4792C32.5988 26.434 32.5729 26.4012 32.5532 26.3815C32.534 26.3621 32.5049 26.3293 32.4656 26.2841C32.4267 26.239 32.4005 26.2094 32.3874 26.1963C32.3224 26.1182 32.1727 25.9555 31.9384 25.708C31.7038 25.4608 31.5348 25.2688 31.4306 25.1321C31.3264 24.9953 31.1799 24.8067 30.9912 24.5655C30.8025 24.3249 30.6395 24.0741 30.5028 23.8136C30.3661 23.5533 30.2326 23.2667 30.1024 22.9544C31.7165 22.0161 32.9863 20.8671 33.9101 19.5055Z"/>
                  </svg> Asesórate</a>
                </div>
              </div>
            </div>
            <div class="col-xl-8 col-lg-6">
              <div class="modal___modelo__body__content" style="background-color:#EAE9E3;">
                <div class="owl-carousel owl-modelo__ owl-theme">
                    <div class="item"><img src="images/modelo/interna-1.png" class="img-fluid" alt=""> </div>
                    <div class="item"><img src="images/modelo/interna-1.png" class="img-fluid" alt=""> </div>
                    <div class="item"><img src="images/modelo/interna-1.png" class="img-fluid" alt=""> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- mapa -->
<div class="modal modal__mapa fade" id="modalMapa" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="images/iconos/cerrar.png" alt="">
        </button>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.4044601953424!2d-77.02752968457513!3d-12.084439645894138!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c889cfb41af1%3A0xeb94fbbc507261b9!2sAv.%20Paseo%20de%20la%20Rep%C3%BAblica%202199%2C%20La%20Victoria%2015034!5e0!3m2!1ses-419!2spe!4v1592848111252!5m2!1ses-419!2spe" width="800" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </div>
    </div>
  </div>
</div>

<!-- modal Texto -->
<div class="modal modal__texto fade" id="modalTexto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <img src="images/iconos/cerrar.png" alt="">
        </button>
        <h2 class="titulo titulo--grande">Transparencia</h2>
        <p class="parrafo">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        <p class="parrafo">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      </div>
    </div>
  </div>
</div>
